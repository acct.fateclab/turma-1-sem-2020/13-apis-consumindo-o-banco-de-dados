# 13 - APIs consumindo o banco de dados

## Desafio

Conforme falamos em aula, estamos em uma fase de escrever o código que outra pessoa entenda e que não basta mais escrever o código que apenas a maquina e você entendem. Dito isso, vamos começar com as APIs!

### Banco de Dados

O banco deverá ter duas entidades: **Aluno**, **Curso**

**Aluno**
- id: string
- nome: string
- sobrenome: string
- idade: number
- idCurso: string (Regra: Um aluno só pode ter um curso)

**Curso**
- id: string
- nome: string
- diasDaSemana: enum (Regra: Sabado e Domingo devem dar erro e apresentar uma mensagem de erro)
  - Segunda
  - Terça
  - Quarta
  - Quinta
  - Sexta

### Endpoints

- Busca de Alunos (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por nome e sobrenome *
  - Por ID do curso *

- Busca de Curso (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por dia da semana *

- Cadastro de Alunos
- Cadastro de Cursos

- Alterar aluno de curso (enviar os IDs dos registros)
- Alterar curso para outro dia da semana (enviar os IDs dos registros) *

`* Endpoints para se destacar`

Para a entrega, **exportem** o banco de dados **já populado** para podermos realizar testes mais facilmente e coloquem no repositório.
